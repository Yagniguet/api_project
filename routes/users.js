const express = require('express')
const router = express.Router()
const usersController = require('../controllers/users')

router.post('', usersController.create)
router.get('/', usersController.all)
router.get('/:id', usersController.one)
router.put('/:id', usersController.update)
router.delete('/:id', usersController.remove)

module.exports = router
