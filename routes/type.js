const express = require('express')
const router = express.Router()
const typeController = require('../controllers/type')

router.post('', typeController.create)
router.get('/', typeController.all)
router.get('/:id', typeController.one) 
router.put('/:id', typeController.update)
router.delete('/:id', typeController.remove)

module.exports = router