const userService = require('../service/users')

const create = (req, res) =>{
    userService.create(req,res)
}

const all = (req, res) =>{
    userService.all(req,res)
}

const one = (req, res) =>{
    userService.one(req,res)
}

const update = (req, res) =>{
    userService.update(req,res)
}

const remove = (req, res) =>{
    userService.remove(req,res)
}

module.exports = {create, all, one, update, remove}