const typeService = require('../service/type')

const create = (req, res) =>{
    typeService.create(req,res)
}

const all = (req, res) =>{
    typeService.all(req,res)
}

const one = (req, res) =>{
    typeService.one(req,res)
}

const update = (req, res) =>{
    typeService.update(req,res)
}

const remove = (req, res) =>{
    typeService.remove(req,res)
}

module.exports = {create, all, one, update, remove}