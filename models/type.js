const mongoose = require('mongoose');
const Schema = mongoose.Schema; //Extraire schema de mongoose

//new schema prend en paramètre un objet
const typeSchema = new Schema({
    libelle : {type:String, default: "admin", required:true},
    shortCode : String 
}) 

module.exports = mongoose.model("Type", typeSchema)