const mongoose = require('mongoose')
const {Schema} = mongoose

const userSchema = new Schema({
    typeId: {type: Schema.Types.ObjectId, ref:"Type", required: false}, //relation (clé primaire de type, clé secondaire de user)
    useName : String,
    firstName : {type: String,  required: false},
    lastName : {type: String,  required: false},
    // email : {type: String, required:false, unique : true},
    // preference : {type : []},
    // address : {
    //     rue : {type: String, required : false},
    //             postaleCode : String
    //  }
})
 
module.exports = mongoose.model("Users", userSchema)