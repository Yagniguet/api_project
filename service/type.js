const TypeModel = require('../models/type')

const create = async (req, res) =>{
    const model = new TypeModel(req.body)
    const response = await model.save();
    res.status(200).json(response)
}

const all = async (req,res) =>{
    const response = await TypeModel.find({})
    res.status(200).json(response)
}

const one = async (req,res) => {
    const response = await TypeModel.findOne({_id : req.params.id})
    res.status(200).json(response)
}

const update = async (req,res) => {
    try{
        const response = await TypeModel.findByIdAndUpdate(req.params.id, req.body)
        res.status(200).json(response)
    } catch(e){
        res.status(401).json('data is not find')
    }
}

const remove = async (req, res) =>{
    const response = await TypeModel.findByIdAndDelete(req.params.id)
    res.status(200).json(response)
}


module.exports = {create, all, one, update, remove}