const usersType = require ('../models/users')

const create = async (req, res, next) => {
    const models = new usersType (req.body)
    const response = await models.save()
    res.status(201).json(response)
}

const all = async (req,res) =>{
    const response = await usersType.find({})
    res.status(200).json(response)
}

const one = async (req,res) => {
    const response = await usersType.findOne({_id : req.params.id})
    res.status(200).json(response)
}

const update = async (req,res) => {
    try{
        const response = await usersType.findByIdAndUpdate(req.params.id, req.body)
        res.status(200).json(response)
    } catch(e){
        res.status(401).json('data is not find')
    }
}

const remove = async (req, res) =>{
    const response = await usersType.findByIdAndDelete(req.params.id)
    res.status(200).json(response)
}


module.exports = {create, all, one, update, remove}